import java.util.*;

public class TreeTest
{
   public static void main(String[] args)
   {
      int size = 20;
      int i = 0;
      Integer[] arr = new Integer[size];
      
      Scanner input = new Scanner(System.in);
      
      System.out.println("Enter values: ");
      
      while((i < size) && input.hasNextInt())
      {
         arr[i] = input.nextInt();
         i++;
      }
      
      if(i == 0)
      {
         System.out.println("The tree is empty.");
      }
      else
      {
         if(TreeWork.isHeap(arr, i))
         {
            System.out.println("IS a heap!");
         }
         else
         {
            System.out.println("is NOT a heap...");
         }
         System.out.println();
         
         TreeWork.printTree(arr, i);
      }
      
   }
}
         
         