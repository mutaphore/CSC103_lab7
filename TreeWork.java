public class TreeWork
{
   public static <T extends Comparable<? super T>> boolean isHeap(T[] arr, int N)
   {  
      for(int i = 0; i < N; i++)
      {
         if(((2 * i) + 1) < N)
         {
            if(((2 * i) + 2) >= N)
            {
               if(arr[(2 * i) + 1].compareTo(arr[i]) < 0)
               {
                  return false;
               } 
            }
            else
            {
               if((arr[(2 * i) + 1].compareTo(arr[i]) < 0) || (arr[(2 * i) + 2].compareTo(arr[i]) < 0))
               {
                  return false;
               }
            }
         }
      }
      
      return true;
   }
   
   public static <T> void printTree(T[] arr, int N)
   {
      int k = 0;
      
      for(int i = 0; k < N; i++)
      {
	 for(int j = 0; j < Math.pow(2, i) && (k < N); j++)
         {
            System.out.print(arr[k] + " ");
            k++;
         }
         System.out.println();
      }
   }
     
}    